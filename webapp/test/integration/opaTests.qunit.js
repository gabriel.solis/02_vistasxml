/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Tutoriales/VistaXML/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});