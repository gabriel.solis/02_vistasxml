/*global QUnit*/

sap.ui.define([
	"Tutoriales/VistaXML/controller/PrimeraVista.controller"
], function (Controller) {
	"use strict";

	QUnit.module("PrimeraVista Controller");

	QUnit.test("I should test the PrimeraVista controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});